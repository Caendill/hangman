import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
@Component({
  selector: 'hm-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StartComponent{
}
