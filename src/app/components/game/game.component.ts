import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpWordsService} from '../../services/http-words.service';
import {KeyboardComponent} from './components/keyboard/keyboard.component';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {LostDialogComponent} from '../lost-dialog/lost-dialog.component';
import {WonDialogComponent} from '../won-dialog/won-dialog.component';
import {Router} from '@angular/router';

@Component({
  selector: 'hm-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

  @ViewChild(KeyboardComponent) keyboard?: KeyboardComponent;

  readonly NUMBER_OF_STAGES = 5;
  readonly MAX_ERRORS = 6;

  wordsToGuess: string[] = [];
  currentStage = 0;
  errors = 0;

  startTime = 0;
  displayedWord = '';

  constructor(private wordsService: HttpWordsService,
              private dialog: MatDialog,
              private router: Router) {
  }

  ngOnInit(): void {
    this.wordsService.getWords().subscribe(words => {
      this.wordsToGuess = this.getRandomNElementsFormList(words, this.NUMBER_OF_STAGES);
      this.updateDisplayedWord();
    });

    this.startTime = performance.now();
  }

  letterSelected(letter: string): void {
    if (this.wordsToGuess[this.currentStage].includes(letter)) {
      this.goodLetterGuessed(letter);
    } else {
      this.wrongLetterGuessed();
    }
  }

  showLetter(letter: string): void {
    const updatedWord = Array.from(this.wordsToGuess[this.currentStage])
      .map((l, i) => l === letter ? l : this.displayedWord[i])
      .join('');
    this.displayedWord = updatedWord;
  }

  private updateDisplayedWord(): void {
    const nonWhitespaceRegex = new RegExp('\\S');
    this.displayedWord = Array.from(this.wordsToGuess[this.currentStage])
      .map(letter => letter.replace(nonWhitespaceRegex, '_'))
      .join('');
  }

  private getRandomNElementsFormList<T>(list: T[], numberOfElements: number): T[] {
    return list
      .sort(() => Math.random() - Math.random())
      .slice(0, numberOfElements);
  }

  private goodLetterGuessed(letter: string): void {
    this.showLetter(letter);
    if (!this.displayedWord.includes('_')) {
      if (this.currentStage + 1 === this.NUMBER_OF_STAGES) {
        this.won();
      } else {
        this.currentStage++;
        this.updateDisplayedWord();
        this.keyboard?.clearButtonsSelection();
      }
    }
  }

  private wrongLetterGuessed(): void {
    this.errors++;
    if (this.MAX_ERRORS === this.errors) {
      this.lost();
    }
  }

  private lost(): void {
    const lostDialog = this.dialog.open(LostDialogComponent);

    lostDialog.afterClosed().subscribe(() => {
      this.router.navigate(['start']);
    });
  }

  private won(): void {
    const time = ((performance.now() - this.startTime) / 1000).toFixed(1);
    const dialogConfig: MatDialogConfig = {
      data: {
        time
      }
    };

    const wonDialog = this.dialog.open(WonDialogComponent, dialogConfig);

    wonDialog.afterClosed().subscribe(() => {
      this.router.navigate(['start']);
    });

  }
}
