import {ChangeDetectionStrategy, Component, EventEmitter, Output} from '@angular/core';
import {KeyboardButton} from '../../../../interfaces/keyboard-button.interface';
import {KeyboardConstants} from '../../../../constants/keyboard.constants';

@Component({
  selector: 'hm-keyboard',
  templateUrl: './keyboard.component.html',
  styleUrls: ['./keyboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class KeyboardComponent {

  @Output() selectButton = new EventEmitter<string>();

  rows: KeyboardButton[][] = [
    KeyboardConstants.FIRST_ROW_BUTTONS,
    KeyboardConstants.SECOND_ROW_BUTTONS,
    KeyboardConstants.THIRD_ROW_BUTTONS
  ];

  constructor() {
    this.clearButtonsSelection();
  }

  selectLetter(keyboardButton: KeyboardButton): void {
    if (!keyboardButton.selected) {
      keyboardButton.selected = true;
      this.selectButton.next(keyboardButton.key);
    }
  }

  clearButtonsSelection(): void {
    this.rows.forEach(row => row.forEach(button => button.selected = false));
  }

}
