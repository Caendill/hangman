import {Component, Inject, OnInit} from '@angular/core';
import {DialogData} from '../../interfaces/dialog-data.interface';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'hm-won',
  templateUrl: './won-dialog.component.html',
  styleUrls: ['./won-dialog.component.scss']
})
export class WonDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit(): void {
  }

}
