import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LostDialogComponent } from './lost-dialog.component';

describe('EndComponent', () => {
  let component: LostDialogComponent;
  let fixture: ComponentFixture<LostDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LostDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LostDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
