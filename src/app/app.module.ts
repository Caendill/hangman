import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {StartComponent} from './components/start/start.component';
import {GameComponent} from './components/game/game.component';
import {LostDialogComponent} from './components/lost-dialog/lost-dialog.component';
import {HttpClientModule} from '@angular/common/http';
import {KeyboardComponent} from './components/game/components/keyboard/keyboard.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {WonDialogComponent} from './components/won-dialog/won-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';

@NgModule({
  declarations: [
    AppComponent,
    StartComponent,
    GameComponent,
    LostDialogComponent,
    KeyboardComponent,
    WonDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatButtonModule,
    MatDialogModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
