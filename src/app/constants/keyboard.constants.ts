import {KeyboardButton} from '../interfaces/keyboard-button.interface';

export class KeyboardConstants {


  public static readonly FIRST_ROW_BUTTONS: KeyboardButton[] = [
    {
      key: 'q'
    }, {
      key: 'w'
    }, {
      key: 'e'
    }, {
      key: 'r'
    }, {
      key: 't'
    }, {
      key: 'y'
    }, {
      key: 'u'
    }, {
      key: 'i'
    }, {
      key: 'o'
    }, {
      key: 'p'
    }
  ];

  public static readonly SECOND_ROW_BUTTONS: KeyboardButton[] = [
    {
      key: 'a'
    }, {
      key: 's'
    }, {
      key: 'd'
    }, {
      key: 'f'
    }, {
      key: 'g'
    }, {
      key: 'h'
    }, {
      key: 'j'
    }, {
      key: 'k'
    }, {
      key: 'l'
    }
  ];

  public static readonly THIRD_ROW_BUTTONS: KeyboardButton[] = [
    {
      key: 'z'
    },
    {
      key: 'x'
    },
    {
      key: 'c'
    },
    {
      key: 'v'
    },
    {
      key: 'b'
    },
    {
      key: 'n'
    },
    {
      key: 'm'
    },
  ];
}
