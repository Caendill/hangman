import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StartComponent} from './components/start/start.component';
import {LostDialogComponent} from './components/lost-dialog/lost-dialog.component';
import {GameComponent} from './components/game/game.component';
import {WonDialogComponent} from './components/won-dialog/won-dialog.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/start',
    pathMatch: 'full'
  }, {
    path: 'start',
    component: StartComponent
  }, {
    path: 'game',
    component: GameComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
