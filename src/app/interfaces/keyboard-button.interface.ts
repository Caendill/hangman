export interface KeyboardButton {
  key: string;
  selected?: boolean;
}
