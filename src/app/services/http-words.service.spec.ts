import { TestBed } from '@angular/core/testing';

import { HttpWordsService } from './http-words.service';

describe('HttpWordsService', () => {
  let service: HttpWordsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HttpWordsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
