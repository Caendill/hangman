import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpWordsService {

  constructor(private http: HttpClient) {
  }

  getWords(): Observable<string[]> {
    const url = 'assets/words.json';

    return this.http.get<string[]>(url);
  }
}
